package API_Testing.practice;

import org.testng.annotations.Test;
import io.restassured.RestAssured;

public class authors {

    //TODO
    //Create a new package inside API_Testing call it "practice"
    //Create 1 class call it authors
    //write your own RestAssured Tests
    //1 test to print prettyPrint()
    //1 test to print prettyPeek()
    //api​/v1​/Authors
    //api​/v1​/Authors​/{id}

    @Test
    public void verifyAuthorprettyPeek(){
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
                .prettyPeek();
    }

    @Test
    public void verifyAuthorprettyPrint(){
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
                .prettyPrint();
    }

    // TASK create 1 test to validate the status code for authors





}
