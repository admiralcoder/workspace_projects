package API_Testing.practice;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

public class activities {

    // Send a GET request https://fakerestapi.azurewebsites.net/api/v1/Activities
    // validate status code
    // validates content type
    // validate response body = Activity 1

    @Test
    public void validateStatusCodeActivities(){
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .then().statusCode(200);
    }

    @Test
    public void validateContentTyperActivites(){
        RestAssured.given()
                .when() // specify type of request
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .then().contentType(ContentType.JSON);
    }

    @Test
    public void validateResponseBodyActivities(){
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .then()
                .body("title[0]",equalTo("Activity 1"));
    }


    // Write a test to assert Activity 15
    @Test
    public void validateActivity15(){
        RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .then()
                .body("title[14]", equalTo("Activity 15"));
    }
}
