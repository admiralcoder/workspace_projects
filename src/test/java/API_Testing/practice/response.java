package API_Testing.practice;

import io.restassured.RestAssured;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.response.Response;

public class response {

    // Saving the Response Body in a response Interface
    // Use Assertion from TESTNG

    /*
    Extract().Response();
      {
    "id": 1,
    "title": "Activity 1",
    "dueDate": "2021-10-12T05:24:31.1724157+00:00",
    "completed": false
      }
     */
    @Test
    public void validateBody(){
       Response response =  RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities")
                .then()
                .extract()
                .response();

       System.out.println(response.prettyPrint());
        Assert.assertEquals("Activity 1",response.jsonPath().getString("title[0]"));
    }

    // Use TestNG Assertions to validate Response body: ID-TITLE-COMPLETED for each request

    // Practice: Send a GET request for "https://fakerestapi.azurewebsites.net/api/v1/Activities/4"
    /*
    {
  "id": 7,
  "title": "Activity 7",
  "dueDate": "2021-10-16T21:18:42.4849011+00:00",
  "completed": false
    }
     */
    @Test
    public void validateActivities4(){
        Response respose = RestAssured.given()
                .when()
                .get("https://fakerestapi.azurewebsites.net/api/v1/Activities/4")
                .then()
                .extract()
                .response();

        Assert.assertEquals(4 ,respose.jsonPath().getInt("id"));
    }

    // Practice: Send a GET request for "https://fakerestapi.azurewebsites.net/api/v1/Activities/7"

    // Practice: Send a GET request for "https://fakerestapi.azurewebsites.net/api/v1/Activities/10"

    // Practice: Send a GET request for "https://fakerestapi.azurewebsites.net/api/v1/Activities/12"


}












