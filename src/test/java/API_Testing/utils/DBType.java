package API_Testing.utils;

public enum DBType {
    POSTGRESQL, ORACLE, MYSQL, MONGDB, MARIADB
}
