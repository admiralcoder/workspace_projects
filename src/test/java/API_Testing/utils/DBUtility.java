package API_Testing.utils;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBUtility {
    // Set up our variables so we can use it across the class DBUtility
    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;

    //                                            janusGraph
    public static void establishConnection(DBType dbType) {
        try {
            switch (dbType) {
                case MYSQL:
                    break;
                case POSTGRESQL:
                    connection = DriverManager.getConnection(ConfigurationReader.getProperty("demoHR"),
                            ConfigurationReader.getProperty("dbUserName"),
                            ConfigurationReader.getProperty("dbPassword"));
                    if (connection != null) {
                        System.out.println("PostgreSQL DB connection Successfull!");
                    } else {
                        System.out.println("Failed to connect to PostgreSQL Database!");
                    }
                    break;
                case MONGDB:
                    break;
                case MARIADB:
                    break;
                case ORACLE:
                    break;
                default:
                    connection = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeConnections() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
            System.out.println("DB closed successfully!");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //                                                   "select * from employees"
    public static List<Map<String, Object>> getQueryResults(String query) {
        try {
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery(query);

            List<Map<String, Object>> list = new ArrayList<>();
            // Metadata is the data describing the data that is stored in your Data source
            // Metadata generally includes the name, size, and number of rows for each table present
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            //                  getColumnCount returns int value = 5 column
            int columnCount = resultSetMetaData.getColumnCount();

            while (resultSet.next()) {
                Map<String, Object> rowMap = new HashMap<>();
                // iterate column by column until the end
                for (int col = 1; col <= columnCount; col++) {
                    // rowMap.put will add key value into the MAP
                    rowMap.put(resultSetMetaData.getColumnName(col), resultSet.getObject(col));
                    // Example of rowMap.put KEY = VALUE
//                    rowMap.put("emp_no", 10001);
//                    rowMap.put("emp_no", 10002);
//                    rowMap.put("emp_no", 10003);
//                    rowMap.put("emp_no", 10004);
                }
                list.add(rowMap);
            }
            return list;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }


}
