package API_Testing.API_day1;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class logInVerification {

    // Verify using the full url https://api.octoperf.com/public/users/login?password=test12&username=tla.jiraone@gmail.com
    @Test
    public void fullurlVerification(){
        // TODO
        // Send a Post request for octoper log in
        // Assert Status code 200
        // Verify Content-Type

        RestAssured.given().when()
                .post("https://api.octoperf.com/public/users/login?password=test12&username=tla.jiraone@gmail.com")
                .then().assertThat().statusCode(200).and().assertThat().contentType(ContentType.JSON);
    }

    // Verify the API using Map
    // Map stores items in "key and value" pair.
    @Test
    public void loginWithMap(){
        RestAssured.baseURI = "https://api.octoperf.com";
        String path = "/public/users/login";

        //TODO
        //Write a MAP that takes String, Object
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("username","tla.jiraone@gmail.com");
        map.put("password","test12");

        RestAssured.given()
                .queryParams(map)
                .when()
                .post(path)
                .then().log().headers() // log().headers().--> used to print the response headers
                .assertThat().contentType("application/json");
    }

    @Test
    public void multiplelineQueryParams(){
        RestAssured.baseURI = "https://api.octoperf.com";
        String path = "/public/users/login";

        RestAssured.given()
                .queryParams("passwords","test12","username","tla.jiraone@gmail.com") // multiple params
        .when()
                .post(path)
                .then().assertThat().statusCode(200).contentType(ContentType.JSON);
    }


}
