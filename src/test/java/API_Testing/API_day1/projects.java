package API_Testing.API_day1;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

public class projects {

    public String path;
    String userId;
    String id;
    String projectId;
    public static Map<String, String> variables;

    @BeforeTest
    public String setUpLogInAndToken() {
        RestAssured.baseURI = "https://api.octoperf.com";
        path = "/public/users/login";

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("username", "tla.jiraone@gmail.com");
        map.put("password", "test12");

        return RestAssured.given()
                .queryParams(map)
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .post(path)
                .then() // Allows me to validate response body or content type as well as status codes
                .statusCode(200)
                .extract() // Getting all the values from the api POST request
                .body() // {"token": "123124124124jkk124"}
                .jsonPath() // Allows us to navigate in the Json body
                .get("token");  // result = 834f9392-ce89-411c-8366-00c86886ec9b
    }

    // Verify some values from memberOf-> response body "https://api.octoperf.com/workspaces/member-of"
    // Swagger contains same request and response body will include ID, UserId.

    @Test
    public void memberOf() {
        Response response = RestAssured.given()
                .header("Authorization", setUpLogInAndToken())
                .when()
                .get("/workspaces/member-of")
                .then()
                .extract()
                .response();

        System.out.println(response.prettyPrint());
        // ADD assertion for
        // id, name, userId, description
        Assert.assertEquals(200, response.statusCode());
        Assert.assertEquals("Default", response.jsonPath().getString("name[0]"));
        Assert.assertEquals("wAf7I3wB10t72x7FjLsY", response.jsonPath().getString("id[0]"));
        Assert.assertEquals("1kt3tHgB6T29TqnSCje3", response.jsonPath().getString("userId[0]"));
        Assert.assertEquals("", response.jsonPath().getString("description[0]"));

        userId = response.jsonPath().getString("userId[0]"); // Storing userId in a String variable 1kt3tHgB6T29TqnSCje3
        id = response.jsonPath().getString("id[0]"); // Storing Id in a String variable wAf7I3wB10t72x7FjLsY

        // Store variables id and userId into a hashMap which we will use in other requests
        variables = new HashMap<String, String>();
        variables.put("userId", userId);
        variables.put("workspaceId", id);
    }

    // url to conver Json into a Single line String
    // https://tools.knowledgewalls.com/jsontostring

    @Test(dependsOnMethods = {"memberOf"})
    public void createProject() {
        String requestBody = "{\"id\":\"\",\"created\":\"2022-10-10T15:39:40.663Z\",\"lastModified\":\"2021-10-10T15:39:40.663Z\",\"userId\":\"" + variables.get("userId") + "\",\"workspaceId\":\"" + variables.get("workspaceId") + "\",\"name\":\"Alex TLA Project\",\"description\":\"New Project\",\"type\":\"DESIGN\",\"tags\":[]}";

        // interface Response
        Response response = RestAssured.given()
                .headers("Content-Type", "application/json") // authorized to be sent as application/json
                .header("Authorization", setUpLogInAndToken()) //token
                .and()
                .body(requestBody)
                .when()
                .post("https://api.octoperf.com/design/projects")
                .then()
                .extract()
                .response();

        // Modify the request and Assert the following response:
        // type, description, name, userId, workspaceID
        // Also incluse status code and Content-type
//        System.out.println(response.prettyPrint());

        Assert.assertEquals("DESIGN", response.jsonPath().getString("type"));
        Assert.assertEquals("New Project", response.jsonPath().getString("description"));
        Assert.assertEquals("Alex TLA Project", response.jsonPath().getString("name"));
        Assert.assertEquals("1kt3tHgB6T29TqnSCje3", response.jsonPath().getString("userId"));
        Assert.assertEquals("wAf7I3wB10t72x7FjLsY", response.jsonPath().getString("workspaceId"));

        projectId = response.jsonPath().get("id"); //okcyinwBGVru019FaoDF
        /*
        {
    "created": 1665416380663,
    "description": "demo Swagger",
    "id": "okcyinwBGVru019FaoDF",
    "lastModified": 1634406132420,
    "name": "testMultipleprojects",
    "tags": [],
    "type": "DESIGN",
    "userId": "1kt3tHgB6T29TqnSCje3",
    "workspaceId": "wAf7I3wB10t72x7FjLsY"
        }
         */

    }

    // METHOD POST
    // Update existing project
    @Test(dependsOnMethods = {"memberOf","createProject"})
    public void updateProjectName() {
        String requestBody = "{\"created\":1633880380663,\"description\":\"Java is over guys\",\"id\":\"" + projectId + "\",\"lastModified\":1633887083271,\"name\":\"\",\"tags\":[],\"type\":\"DESIGN\",\"userId\":\"" + variables.get("userId") + "\",\"workspaceId\":\"" + variables.get("workspaceId") + "\"}";

        Response response = RestAssured.given()
                .header("content-type", "application/json")
                .header("Authorization", setUpLogInAndToken())
                .and()
                .body(requestBody)
                .when()
                .put("/design/projects/" + projectId)// /design/projects/okcyinwBGVru019FaoDF
                .then()
                .extract()
                .response();

        System.out.println(response.prettyPrint());

        // Assert the following response:
        // type, description, name, userId, workspaceID
        // Also incluse status code and Content-type
    }

    // METHOD DELETE project https://api.octoperf.com/design/projects/{{id}}
    @Test(dependsOnMethods = {"memberOf", "createProject", "updateProjectName"})
    public void deleteProject(){
        Response response = RestAssured.given()
                .header("Authorization", setUpLogInAndToken())
                .when()
                .delete("design/projects/"+projectId)
                .then()
                .extract()
                .response();

        Assert.assertEquals(204, response.statusCode());
        System.out.println(response.getStatusLine());
    }
}