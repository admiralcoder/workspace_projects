package API_Testing.API_day1;

import io.restassured.RestAssured;
import org.testng.annotations.Test;


public class introRestAssured {

    // Most of the used methods from Rest-Assured library when writing tests are:
    // When making a request:
        // given() --> used to prepare the request
        // when() and get() or just when() --> used to send the request
        // Then() --> used to verify request

    // To verify Response:
        // prettyPeek() --> used to print all the response
        // prettyprint() --> print only response body
        // log() --> print all the response
        // contentType() --> used to verify body and response header in Json. when we use POST request.
        // accept() --> used to verify the body response header when making a GET request


    // in order to make a request need to identify the baseURI
    // baseURI --> saves the base url for all resources

    public static String baseURI = "https://api.octoperf.com";

    // when we make a request we provide the path(endpoint) to a specific base resource(url)
    private String path = "public/users/login";

    // the full path for the URL/endpoint will be --> https://api.octoperf.com/public/users/login


    /*
    Make a POST request with the given full url "https://api.octoperf.com/public/users/login?password=test12&username=tla.jiraone%40gmail.com"
    Print the Entire Response using prettyPrint(), prettyPeek();
     */

    @Test
    public void printTheResponse(){
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?password=test12&username=tla.jiraone@gmail.com")
                .prettyPeek(); // prints the entire response body and header
    }

    @Test
    public void printTheResponseprettyPrint(){
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?password=test12&username=tla.jiraone@gmail.com")
                .prettyPrint(); // prints the entire response body
    }

    /*
    When we verify the status code we must pay attention to the following errors from the response:

    1xx --> information
    2xx --> success(200-> OK, 201-> Created, 204-> No content)
    3xx --> redirect
    4xx --> client error (400 -> Bad Request, 401 -> Unauthorized, 403 -> Forbidden, 404 -> Not Found, 405 -> method not allowed)
    5xx --> server error (500 -> Internal server error,  502 -> Bad gateway, 503 -> Service unavailable)
     */

    // Validate Status codes
    @Test
    public void verifyStatusCode(){
        RestAssured.given()
                .when()
                .post("https://api.octoperf.com/public/users/login?password=test12&username=tla.jiraone@gmail.com")
                .then()
                .assertThat().statusCode(200);
    }




    //Class will start at  7:05 PM
    //Open Jira

    // OPEN your RestAssured project
    // Open postman
    // open gitlab
























}
