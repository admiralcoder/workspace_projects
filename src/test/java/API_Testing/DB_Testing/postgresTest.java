package API_Testing.DB_Testing;

import API_Testing.utils.DBType;
import API_Testing.utils.DBUtility;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class postgresTest {

    @BeforeMethod
    public void setUp(){
        DBUtility.establishConnection(DBType.POSTGRESQL);
    }

    @AfterMethod
    public void closeDB(){
        DBUtility.closeConnections();
    }

    @Test
    public void assertDB(){
       List<Map<String, Object>> titlesResult = DBUtility.getQueryResults("SELECT * FROM titles limit 10");

        Assert.assertEquals(titlesResult.get(1).get("title"),"Staff");
        Assert.assertEquals(titlesResult.get(4).get("title"), "Senior Engineer");
    }
}
