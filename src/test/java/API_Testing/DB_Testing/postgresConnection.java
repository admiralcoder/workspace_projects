package API_Testing.DB_Testing;

import org.testng.annotations.Test;

import java.sql.*;

public class postgresConnection {
    //Path for PostgreSQL database: url link to set up the connection
    String demoHR = "jdbc:postgresql://localhost:5432/demoHR";

    // Username
    String dbUserName = "postgres";

    // password
    String dbPassword = "";

    @Test
    public void printAllFromdepartments() throws SQLException {
        // Establish a DBTYPE connection
        // Connect to database with JDBC for this we use Interface Connection
        Connection connection = DriverManager.getConnection(demoHR,dbUserName,dbPassword);
        // The Statement interface allows us to read data from the database
        Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        // ResultSet interface allows us to execute and save the query in resultset object to make manipulation within tests
        ResultSet resultSet = statement.executeQuery("SELECT * FROM departments");

        // next() --> checks if there is any data in the database
        while(resultSet.next()){
            // print the column with index 1
//            System.out.println(resultSet.getString(1));
//            System.out.println(resultSet.getString("dept_no"));
            System.out.println(resultSet.getString("dept_name"));
        }
        resultSet.close();
        statement.close();
        connection.close();
    }

}
